<?php

namespace App\Http\Controllers;

use App\Models\ObjectManager;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Auth;

class ObjectManagerController extends Controller {

  private $path = 'object_manager';
  
  public function __construct() {
    $this->middleware('auth');
  }

  public function getUserRoleName () {
    $usr_id = Auth::id();
    $usr_env_id = Auth::user()->enviroment_id;
    $user_role_name = DB::select('call get_cur_user_type_name_by_usr_id('. $usr_env_id.', '. $usr_id .')')[0]->role_type;

    return $user_role_name;
  }

  public function index(){
    $usr_id = Auth::id();
    $usr_env_id = Auth::user()->enviroment_id;
    $user_role_name = $this->getUserRoleName();
    $object_data = json_encode(DB::select('call get_all_objects_by_env_id('. $usr_env_id .')'));
    return view($this->path . '.index', compact('object_data', 'user_role_name'));
  }

  /**
   * Show the form for creating a new resource.
   *
   * @return \Illuminate\Http\Response
   */
  public function create(){
    $user_role_name = $this->getUserRoleName();
    return view($this->path . '.create', compact('user_role_name'));
  }

  /**
   * Store a newly created resource in storage.
   *
   * @param  \Illuminate\Http\Request  $request
   * @return \Illuminate\Http\Response
   */
  public function store(Request $request)
  {
      //
  }

  /**
   * Display the specified resource.
   *
   * @param  \App\Models\ObjectManager  $objectManager
   * @return \Illuminate\Http\Response
   */
  public function show(ObjectManager $objectManager)
  {
      //
  }

  /**
   * Show the form for editing the specified resource.
   *
   * @param  \App\Models\ObjectManager  $objectManager
   * @return \Illuminate\Http\Response
   */
  public function edit(ObjectManager $objectManager)
  {
      //
  }

  /**
   * Update the specified resource in storage.
   *
   * @param  \Illuminate\Http\Request  $request
   * @param  \App\Models\ObjectManager  $objectManager
   * @return \Illuminate\Http\Response
   */
  public function update(Request $request, ObjectManager $objectManager)
  {
      //
  }

  /**
   * Remove the specified resource from storage.
   *
   * @param  \App\Models\ObjectManager  $objectManager
   * @return \Illuminate\Http\Response
   */
  public function destroy(ObjectManager $objectManager)
  {
      //
  }
}
