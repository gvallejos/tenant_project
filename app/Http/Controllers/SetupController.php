<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Auth;

class SetupController extends Controller {
  private $path = 'setup';
  
  public function __construct() {
    $this->middleware('auth');
  }
    
  public function index() {
    $usr_id = Auth::id();
    $usr_env_id = Auth::user()->enviroment_id;
    $user_role_name = DB::select('call get_cur_user_type_name_by_usr_id('. $usr_env_id.', '. $usr_id .')')[0];
    /*
    $users_data = json_encode(DB::select('call get_all_users_by_env_id('. $usr_env_id .')'));
    $object_data = json_encode(DB::select('call get_all_objects_by_env_id('. $usr_env_id .')'));
    
    $enviroments_data = json_encode(DB::select('call get_all_enviroments()'));
    return view(
                $this->path, 
                compact(
                  'users_data',
                  'object_data',
                  'user_role_name',
                  'enviroments_data'
                )
              );
    */
    return view($this->path . '.index', compact('user_role_name'));
  }
}
