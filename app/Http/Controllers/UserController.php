<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Auth;

class UserController extends Controller {
  private $path = 'user';
  
  public function __construct() {
    $this->middleware('auth');
  }

  public function index () {
    $usr_id = Auth::id();
    $usr_env_id = Auth::user()->enviroment_id;
    $users_data = json_encode(DB::select('call get_all_users_by_env_id('. $usr_env_id .')'));
    $user_role_name = DB::select('call get_cur_user_type_name_by_usr_id('. $usr_env_id.', '. $usr_id .')')[0];
    return view($this->path . '.index', compact('users_data', 'user_role_name'));
  }
}
