@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row">
      <div class="col-md-4">
        <div class="nav flex-column nav-pills" id="v-pills-tab" role="tablist" aria-orientation="vertical">
          <a class="nav-link active" 
             id="v-pills-settings-tab"
             href="{{ route('setup.index') }}" >
            Setup
          </a>
          <a class="nav-link" 
             id="v-pills-users-tab"
             href="{{ route('user.index') }}" >
            Users
          </a>
          <a class="nav-link" 
             id="v-pills-object-manager-tab"
             href="{{ route('object-manager.index') }}" >
            Object Manager
          </a>
          @if ($user_role_name->role_type == 'Propietario')
          <a class="nav-link" 
             id="v-pills-enviroments-tab"
             href="{{ route('enviroment.index') }}" >
            Enviroments
          </a>
          @endif          
        </div>
      </div>
      <div class="col-md-8">
        <h3>Setup Description</h3>
        <ul>
          <li>Information about the org (Property).</li>
          <li>Information about the enviroment for the user (User).</li>
        </ul>
      </div>
    </div>
</div>
@endsection